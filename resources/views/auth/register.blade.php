@extends('layouts.guest')

@section('content')
    <div class="flex min-h-screen text-white">
        <div class="flex flex-none justify-center md:w-1/4 w-full min-h-screen flex-1 bg-gray-800">
            <div class="flex flex-col min-h-screen justify-center w-3/4">
                <h2 class="text-3xl font-extrabold my-8">{{ __('Create your account') }}</h2>
                <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="mt-4">
                    <x-label for="name" :value="__('Name')" />
                    <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                </div>

                <div class="mt-4">
                    <x-label for="email" :value="__('Email')" />
                    <x-input id="email" class="block mt-1 w-full" type="text" name="email" :value="old('email')" required />
                </div>

                <div class="mt-4">
                    <x-label for="password" :value="__('Password')" />
                    <x-input id="password" class="block mt-1 w-full" type="password" name="password" :value="old('password')" required />
                </div>

                <div class="mt-4">
                    <x-label for="password_confirmation" :value="__('Confirm password')" />
                    <x-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required />
                </div>

                <div class="mt-4">
                    <x-button>
                        {{ __('Register') }}
                    </x-button>
                </div>
            </form>
            <div class="mt-8">
                <a class="text-sm text-blue-600 hover:text-blue-800" href="{{ route('login') }}">
                    {{ __('Already registered ?') }}
                </a>
            </div>
            </div>
        </div>
        <div class="relative hidden flex-1 w-0 bg-green-500 md:block">
            <img class="absolute inset-0 object-cover w-full h-full" alt="Background" src="/images/desktop.jpg">
        </div>
    </div>
@endsection
