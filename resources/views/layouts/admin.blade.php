<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    </head>
    <body class="antialiased bg-gray-100 flex" style="font-family: 'Roboto', sans-serif;">
        <aside class="relative bg-blue-600 h-screen w-64">
            <div class="w-full text-center py-8 text-3xl text-white uppercase font-semibold">{{ __('Admin') }}</div>
            <nav class="text-white">
                <a href="{{ route('admin.users.index') }}" class="{{ Request::is('admin/users*') ? 'bg-blue-800 opacity-100' : '' }} flex items-center opacity-80 hover:opacity-100 hover:bg-blue-800 py-4 pl-6">
                    <svg class="h-6 w-6 mr-3" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
                    </svg>
                    {{ __('Users') }}
                </a>
            </nav>
        </aside>
        <div class="relative w-full h-screen">
            <header class="w-full bg-white border-b border-gray-200 py-2 px-6">
                Header
            </header>
            <main class="w-full p-6">
                @yield('content')
            </main>
        </div>
        <script src="{{ asset('js/app.js') }}" defer></script>
    </body>
</html>
