@extends('layouts.guest')

@section('content')
    <div class="flex min-h-screen text-white">
        <div class="flex flex-none justify-center md:w-1/4 w-full min-h-screen flex-1 bg-gray-800">
            <div class="flex flex-col min-h-screen justify-center w-3/4">
                <h2 class="text-3xl font-extrabold my-8">{{ __('Welcome home') }} {{ auth()->user()->name }}</h2>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <div class="mt-4">
                        <button class="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 rounded shadow w-full duration-200" type="submit">
                            {{ __('Sign out') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="relative hidden flex-1 w-0 bg-green-500 md:block">
            <img class="absolute inset-0 object-cover w-full h-full" alt="Background" src="/images/desktop.jpg">
        </div>
    </div>
@endsection

