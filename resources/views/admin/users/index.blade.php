@extends('layouts.admin')

@section('content')
    <h1 class="text-3xl text-black pb-6">{{ __('Users') }}</h1>
    <table class="min-w-full bg-white">
        <thead class="bg-gray-800 text-white">
            <tr>
                <th class="py-3 px-4 text-left uppercase font-semibold text-sm">{{ __('Name') }}</th>
                <th class="py-3 px-4 text-left uppercase font-semibold text-sm">{{ __('Email') }}</th>
                <th class="py-3 px-4 text-left uppercase font-semibold text-sm">{{ __('Created at') }}</th>
                <th class="py-3 px-4 text-left uppercase font-semibold text-sm">{{ __('Updated at') }}</th>
                <th class="py-3 px-4 text-left uppercase font-semibold text-sm">{{ __('Role') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr class="{{ $loop->even ? 'bg-gray-200' : ''  }}">
                    <td class="py-3 px-4 text-left"><a href="{{ route('admin.users.edit', $user) }}" class="text-blue-500 hover:text-blue-800 underline">{{ $user->name }}</a></td>
                    <td class="py-3 px-4 text-left">{{ $user->email }}</td>
                    <td class="py-3 px-4 text-left">{{ $user->created_at->format('d/m/Y') }}</td>
                    <td class="py-3 px-4 text-left">{{ $user->updated_at->format('d/m/Y') }}</td>
                    <td class="py-3 px-4 text-left">{{ $user->role }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
