@extends('layouts.admin')

@section('content')
    <h1 class="text-3xl text-black pb-6">{{ __('Edit user') }}</h1>
    <form class="p-10 bg-white rounded shadow-lg" method="POST" action="{{ route('admin.users.update', $user) }}">
        @csrf
        @method('patch')
        <div>
            <x-label for="name" :value="__('Name')" />
            <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name', $user->name)"/>
        </div>
        <div class="mt-4">
            <x-label for="Email" :value="__('Email')" />
            <x-input  id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email', $user->email)"/>
        </div>
        <div class="mt-4">
            <x-label for="role" :value="__('Role')" />
            <x-select id="role" name="role" :options="['user' => __('User'), 'admin' => __('Admin')]" :value="old('role', $user->role)"/>
        </div>
        <x-button>
            {{ __('Submit') }}
        </x-button>
    </form>
@endsection
