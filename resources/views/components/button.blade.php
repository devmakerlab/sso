<button {{ $attributes->merge(['type' => 'submit', 'class' => 'bg-blue-500 hover:bg-blue-800 text-white font-bold py-2 rounded shadow w-full duration-200 mt-8']) }}>
    {{ $slot }}
</button>
