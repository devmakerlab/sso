@props(['name', 'options', 'value'])

<select name="{{ $name }}"
    {!! $attributes->merge(['class' => 'block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-300 rounded-md focus:border-blue-500 focus:outline-none']) !!}
>
    @foreach($options as $key => $option)
        <option value="{{ $key }}" {{ $value === $key ? 'selected' : null }}>{{ $option }}</option>
    @endforeach
</select>
@error($name)
    <span class="text-red-500 text-sm font-semibold"> {{ $message }}</span>
@enderror
