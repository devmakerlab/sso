<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('welcome');
})->middleware('auth');

Route::get('/', [App\Http\Controllers\Auth\AuthController::class, 'loginForm'])
    ->middleware('guest')
    ->name('login');

Route::post('login', [App\Http\Controllers\Auth\AuthController::class, 'login'])
    ->middleware('guest');

Route::post('logout', [App\Http\Controllers\Auth\AuthController::class, 'logout'])
    ->middleware('auth')
    ->name('logout');

Route::get('/register', [App\Http\Controllers\Auth\RegisterController::class, 'registerForm'])
    ->middleware('guest')
    ->name('register');

Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'store'])
    ->middleware('guest');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'is.admin']], function () {
    Route::get('users', [App\Http\Controllers\Admin\UsersController::class, 'index'])
        ->name('admin.users.index');

    Route::get('users/{user}/edit', [App\Http\Controllers\Admin\UsersController::class, 'edit'])
        ->name('admin.users.edit');

    Route::patch('users/{user}', [App\Http\Controllers\Admin\UsersController::class, 'update'])
        ->name('admin.users.update');
});
