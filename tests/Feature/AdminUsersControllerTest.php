<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminUsersControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_list_users()
    {
        $admin = User::factory()->create(['role' => 'admin']);

        $users = User::factory()->count(5)->create();

        $response = $this->actingAs($admin)->get('/admin/users');

        $response->assertSuccessful();
        $response->assertSee('Users');
        $response->assertSeeTextInOrder($users->pluck('name')->toArray());
    }

    /** @test */
    public function user_cant_list_users()
    {
        $user = User::factory()->create(['role' => 'user']);

        $response = $this->actingAs($user)->get('/admin/users');

        $response->assertForbidden();
    }

    /** @test */
    public function guest_cant_list_users()
    {
        $response = $this->get('/admin/users');

        $response->assertRedirect('/');
    }

    /** @test */
    public function admin_can_edit_user()
    {
        $admin = User::factory()->create(['role' => 'admin']);

        $target = User::factory()->create([
            'email' => 'old@example.org',
        ]);

        $response = $this
            ->from('/admin/users/' . $target->id)
            ->actingAs($admin)
            ->patch('/admin/users/' . $target->id, [
                'email' => $email = 'target@example.org',
            ]);

        $response->assertRedirect('/admin/users/' . $target->id);

        $this->assertDatabaseHas('users', [
            'email' => $email,
        ]);
    }

    /** @test */
    public function user_cant_edit_user()
    {
        $user = User::factory()->create(['role' => 'user']);

        $target = User::factory()->create();

        $response = $this
            ->from('/admin/users/' . $target->id)
            ->actingAs($user)
            ->patch('/admin/users/' . $target->id, [
                'email' => 'no-way@example.org',
            ]);

        $response->assertForbidden();
    }

    /** @test */
    public function guest_cant_edit_user()
    {
        $target = User::factory()->create();

        $response = $this
            ->from('/admin/users/' . $target->id)
            ->patch('/admin/users/' . $target->id, [
                'email' => 'no-way@example.org',
            ]);

        $response->assertRedirect('/');
    }
}
