<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class RegisterControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function auth_user_is_redirect_on_register_form()
    {
        $user = User::factory()->create();

        $response = $this
            ->from('/around-the-world')
            ->actingAs($user)
            ->get('/register');

        $response->assertRedirect('/home');
    }

    /** @test */
    public function guest_can_register()
    {
        $response = $this->post('/register', [
            'name' => 'John Doe',
            'email' => 'guest@devmaker.tv',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $this->assertDatabaseHas('users', [
            'email' => 'guest@devmaker.tv',
        ]);

        $this->assertAuthenticated();
        $response->assertRedirect('/home');
    }

    /** @test */
    public function guest_cant_register_with_existing_email()
    {
        User::factory()->create([
            'email' => 'guest.again@devmaker.tv'
        ]);

        $response = $this->from('/register')->post('/register', [
            'name' => 'John Redoe',
            'email' => 'guest.again@devmaker.tv',
            'password' => 'password',
            'password_confirmation' => 'password',
        ]);

        $this->assertGuest();
        $response->assertSessionHasErrors(['email']);
        $response->assertRedirect('/register');
    }
}
