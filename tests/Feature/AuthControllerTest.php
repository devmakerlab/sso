<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    public function guest_can_login()
    {
        $user = User::factory()->create();

        $response = $this->from('/arount-the-world')->post('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $this->assertAuthenticatedAs($user);
        $response->assertRedirect('/arount-the-world');
    }

    /** @test */
    public function auth_user_is_redirect_on_login_form()
    {
        $user = User::factory()->create();

        $response = $this
            ->from('/around-the-world')
            ->actingAs($user)
            ->get('/');

        $response->assertRedirect('/home');
    }

    /** @test */
    public function guest_cant_login_with_wrong_credentials()
    {
        $response = $this->from('/login')->post('/login', [
            'email' => 'hacker@noway.com',
            'password' => 'WRONGpassword',
        ]);

        $this->assertGuest();
        $response->assertSessionHasErrors(['email']);
        $response->assertRedirect('/login');
    }
}
